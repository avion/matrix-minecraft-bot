import subprocess
import asyncio

from matrix_minecraft_bot.chat_functions import send_text_to_room


async def server_process_loop(client, room_id, process):
    await asyncio.create_subprocess_exec("docker", process, "mc-cf-server")
    if process is "stop":
        return

    # Give docker 20 seconds to start the process
    await asyncio.sleep(20)
    # Unintuitive, but the seconds below are multiplied by 5.
    # So timeout of roughly 5.5 minutes.
    start_timeout = 66
    temp_time = 0
    process_complete = False
    data = None

    while process is not "stop" and process_complete is False:
        temp_time += 1
        print(f"temp_time: {temp_time}")

        status = await asyncio.create_subprocess_exec(
            "docker",
            "inspect",
            "--format",
            "'{{json .State.Health.Status }}'",
            "mc-cf-server",
            stdout=asyncio.subprocess.PIPE)

        data = await status.stdout.readline()
        print(str(data))

        if "unhealthy" not in str(data) and "healthy" in str(data):
            process_complete = True
        elif temp_time <= start_timeout:
            await asyncio.sleep(5)
            continue
        else:
            print("Entered else")
            break
    if "unhealthy" not in str(data) and "healthy" in str(data):
        text = "Server startup complete."
    elif "unhealthy" in str(data):
        cmd = "docker logs --tail 2000 mc-cf-server | haste"
        ps = await asyncio.create_subprocess_shell(
            cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)

        output = await ps.communicate()
        output = output[0]

        text = "Server still not up. Maybe in a reboot loop? Here's the last 2000 lines of the server log: " + str(
            output).split("'")[1::2][0]
        print(text)

    await send_text_to_room(client, room_id, text)


class Command(object):
    def __init__(self, client, store, config, command, room, event):
        """A command made by a user

        Args:
            client (nio.AsyncClient): The client to communicate to matrix with

            store (Storage): Bot storage

            config (Config): Bot configuration parameters

            command (str): The command and arguments

            room (nio.rooms.MatrixRoom): The room the command was sent in

            event (nio.events.room_events.RoomMessageText): The event describing the command
        """
        self.client = client
        self.store = store
        self.config = config
        self.command = command
        self.room = room
        self.event = event
        self.args = self.command.split()[1:]

    async def process(self):
        """Process the command"""
        if self.command.startswith("echo"):
            await self._echo()
        elif self.command.startswith("help"):
            await self._show_help()
        elif self.command.startswith("server"):
            await self._server()
        elif self.command.startswith("command"):
            await self._command()
        elif self.command.startswith("logs"):
            await self._logs()
        elif self.command.startswith("debug"):
            await self._debug()
        else:
            await self._unknown_command()

    async def _echo(self):
        """Echo back the command's arguments"""
        response = " ".join(self.args)
        await send_text_to_room(self.client, self.room.room_id, response)

    async def _server(self):
        """Start/Stop/Restart docker minecraft container"""
        response = " ".join(self.args)

        if "restart" in response or "reboot" in response:
            await send_text_to_room(self.client, self.room.room_id,
                                    "Restarting minecraft server...")

            asyncio.create_task(
                server_process_loop(self.client, self.room.room_id, "restart"))

        elif "stop" in response:
            await send_text_to_room(self.client, self.room.room_id,
                                    "Stopping minecraft server...")
            asyncio.create_task(
                server_process_loop(self.client, self.room.room_id, "stop"))
        elif "start":
            await send_text_to_room(self.client, self.room.room_id,
                                    "Starting minecraft server...")
            asyncio.create_task(
                server_process_loop(self.client, self.room.room_id, "start"))
        else:
            await send_text_to_room(
                self.client, self.room.room_id,
                "Available commands are `server start`, `server stop`, `server restart`, and `server reboot`"
            )

    async def _logs(self):
        """Send last x lines of the minecraft server's logs"""
        response = " ".join(self.args)

        if not self.args:
            # cmd = "docker logs --tail 1000 mc-cf-server | haste"
            # ps = await asyncio.create_subprocess_shell(
            #     cmd,
            #     stdout=asyncio.subprocess.PIPE,
            #     stderr=asyncio.subprocess.PIPE)

            # output = await ps.communicate()
            # output = output[0]

            # text = "Last 1000 lines of the server log: " + str(output).split(
            #     "'")[1::2][0]
        text = "Live logs URL: \n If you see 'Connection Closed', try reloading the page."
            await send_text_to_room(self.client, self.room.room_id, text)
        elif response.isdigit():
            if int(response) > 2000:
                text = "Please request <= 2000 log lines."
                await send_text_to_room(self.client, self.room.room_id, text)
            elif int(response) is 0:
                text = "...why are you trying to request 0 log lines? Are you " \
                    + "of the stupid?"
                await send_text_to_room(self.client, self.room.room_id, text)
            elif int(response) <= 2000:
                cmd = f"docker logs --tail {response} mc-cf-server | haste"
                ps = await asyncio.create_subprocess_shell(
                    cmd,
                    stdout=asyncio.subprocess.PIPE,
                    stderr=asyncio.subprocess.PIPE)

                output = await ps.communicate()
                output = output[0]
                text = "Last " + response + " lines of the server log: " + str(
                    output).split("'")[1::2][0]
                await send_text_to_room(self.client, self.room.room_id, text)
        else:
            text = "idk how but you botched the command, good job. " \
                + "Remember you can only include numbers after `log`. " \
                + "No, that doesn't include symbols and letters. Stop trying " \
                + "to break my bot Krumble."
            await send_text_to_room(self.client, self.room.room_id, text)

    async def _debug(self):
        """For debugging stuff"""
        response = " ".join(self.args)
        print(str(response))

        if not self.args:
            cmd = "docker logs --tail 1000 mc-cf-server | haste"

            ps = await asyncio.create_subprocess_shell(
                cmd,
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE)

            output = await ps.communicate()
            output = output[0]

            text = "Last 1000 lines of the server log: " + str(output).split(
                "'")[1::2][0]
            await send_text_to_room(self.client, self.room.room_id, text)
        elif response.isdigit():
            if int(response) <= 2000:
                cmd = f"docker logs --tail {response} mc-cf-server | haste"
                ps = await asyncio.create_subprocess_shell(
                    cmd,
                    stdout=asyncio.subprocess.PIPE,
                    stderr=asyncio.subprocess.PIPE)

                output = await ps.communicate()
                output = output[0]
                text = "Last " + response + " lines of the server log: " + str(
                    output).split("'")[1::2][0]
                await send_text_to_room(self.client, self.room.room_id, text)
            else:
                text = "Please request <= 2000 log lines."
                await send_text_to_room(self.client, self.room.room_id, text)
        else:
            text = "idk how but you botched the command, good job. " \
                + "Remember you can only include numbers after `log`." \
                + "No, that doesn't include symbols and letters. Stop trying" \
                + "to break my bot Krumble."
            await send_text_to_room(self.client, self.room.room_id, text)

    async def _command(self):
        """Send rcon-cli commands"""

        response = " ".join(self.args)

        data = subprocess.run(
            ["docker", "exec", "mc-cf-server", "rcon-cli", response],
            capture_output=True)
        #output = data.stdout()
        print(f"Return code: {data.returncode}")
        print(f"stdout: {data.stdout}")
        print(f"stderr: {data.stderr}")

        if "Unknown or incomplete command" in str(data.stdout):
            text = "Unkown or incomplete command."
        else:
            text = f"`{response}` executed successfully."

        await send_text_to_room(self.client, self.room.room_id, text)

    async def _show_help(self):
        """Show the help text"""
        if not self.args:
            text = (
                "Hello, I am a bot to help you restart the minecraft server! Available commands: `restart`"
            )
            await send_text_to_room(self.client, self.room.room_id, text)
            return

        topic = self.args[0]
        if topic == "rules":
            text = "These are the rules!"
        elif topic == "commands":
            text = "restart"
        else:
            text = "Unknown help topic!"
        await send_text_to_room(self.client, self.room.room_id, text)

    async def _unknown_command(self):
        await send_text_to_room(
            self.client,
            self.room.room_id,
            f"Unknown command '{self.command}'. Try the 'help' command for more information.",
        )
